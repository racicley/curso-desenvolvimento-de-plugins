<?php
/*
 * Plugin Name:       RC Slider
 * Plugin URI:        https://www.racicley.com.br
 * Description:       Plugin criado a partir do curso de criação de Plugins.
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Racicley Costa
 * Author URI:        https://www.racicley.com.br
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://www.racicley.com.br
 * Text Domain:       rc-slider
 * Domain Path:       /languages
 */

//Segurança
if(!defined('ABSPATH')){
    die('Silêncio é ouro!');
    exit;
}

//Criando a classe no Wordpress, if verifica se a classe existe
if(!class_exists('RC_Slider')){
    class RC_Slider{
        //construtor
        function __construct(){
            $this->define_constants();
            
            //Adicionando a class do CPT
            require_once(RC_SLIDER_PATH . 'post-types/class.rc-slider-cpt.php');
            $RC_Slider_Post_Type = new RC_Slider_Post_Type();
        }

        public function define_constants(){
            //Caminho pasta plugin
            define('RC_SLIDER_PATH', plugin_dir_path( __FILE__ ));
            //Endereço web
            define('RC_SLIDER_URL', plugin_dir_url( __FILE__ ));
            //Versão do Plugin
            define('RC_SLIDER_VERSION', '1.0.0');
        }

        public static function activate(){
            //Formas de não precisar atualizar os permalinks, update_option funciona melhor, segundo o autor do curso.
            //flush_rewrite_rules();
            update_option( 'rewrite_rules', '');
        }

        public static function deactivate(){
            //Aqui o método funciona bem
            flush_rewrite_rules();
            unregister_post_type( 'rc-slider' );
        }

        public static function uninstall(){

        }
    }
}

if(class_exists('RC_Slider')){
    register_activation_hook( __FILE__, array('RC_SLIDER', 'activate'));
    register_deactivation_hook( __FILE__, array('RC_SLIDER', 'deactivate'));
    register_uninstall_hook( __FILE__, array('RC_SLIDER', 'uninstall'));
    $rc_slider = new RC_Slider();
}